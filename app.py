import os
from flask import Flask, flash, request, redirect, url_for, jsonify
from werkzeug.utils import secure_filename
from flask_cors import cross_origin


UPLOAD_FOLDER = './images'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return 'Hello, World!'

@app.route('/upload', methods=['POST'])
@cross_origin(origins="*")
def upload():
    if request.method == 'POST':
        print(request.files)
        if 'foto' not in request.files or 'wajah' not in request.files or 'ktp' not in request.files:
            resp = jsonify({'message' : 'No file part in the request'})
            resp.status_code = 500
            return resp
        
        foto = request.files['foto']
        wajah = request.files['wajah']
        ktp = request.files['ktp']

        if foto.filename == '' or wajah.filename == '' or ktp.filename == '' :
            resp = jsonify({'message' : 'No file selected'})
            resp.status_code = 500
            return resp
        if foto and allowed_file(foto.filename):
            filename = secure_filename(foto.filename)
            foto.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        
        if wajah and allowed_file(wajah.filename):
            filename = secure_filename(wajah.filename)
            wajah.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        if ktp and allowed_file(ktp.filename):
            filename = secure_filename(ktp.filename)
            ktp.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        
        resp = jsonify({'message' : 'Upload success'})
        resp.status_code = 200
        return resp