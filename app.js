const fileKtp = document.getElementById("file_ktp");
const previewContainer = document.getElementById("ktpPreview");

const previewKTP = previewContainer.querySelector(".image-preview__ktp");
const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

const previewFaceContainer = document.getElementById("facePreview");
const previewFace = previewFaceContainer.querySelector(".face-preview__face");
const previewFaceDefaultText = previewFaceContainer.querySelector(".face-preview__default-text");

const eNIK = document.getElementById("nik");
const eNama = document.getElementById("nama");
const eProses = document.getElementById("proses");

let model;

async function loadModel() {
    model = await blazeface.load();
}

loadModel();

fileKtp.addEventListener("change", function () {
    const file = this.files[0];
    if (file) {
        const reader = new FileReader();



        //console.log(previewKTP);

        reader.addEventListener("load", async function () {
            eNIK.setAttribute("value", "")
            eNama.setAttribute("value", "")
            const image = await loadImage(this.result);
            eProses.style.display = "block";
            Tesseract.recognize(
                image,
                'eng', {
                    logger: m => console.log(m)
                }
            ).then(({
                data: {
                    text
                }
            }) => {
                //console.log(text);
                eProses.style.display = "none";
                const arrayText = text.split("\n");
                //console.log(arrayText);
                const NUMERIC_REGEXP = /[-]{0,1}[\d]*[.]{0,1}[\d]+/g;
                arrayText.forEach((txt) => {
                    const pattNIK = new RegExp("NIK");
                    const tNIK = pattNIK.test(txt)
                    if (tNIK) {
                        //console.log(txt)
                        const nik = txt.match(NUMERIC_REGEXP);
                        //console.log(nik)
                        if (nik.length > 0) {
                            
                            
                            eNIK.setAttribute("value", nik.join(''))
                            //console.log(nik[0])
                        }
                    }

                    const pattNama = new RegExp("Nama");
                    const tNama = pattNama.test(txt)
                    if (tNama) {
                        let nama = txt.replace('Nama','');
                        nama = nama.replace(":", '')
                        nama = nama.replace("-", '')
                        eNama.setAttribute("value", nama)
                        //console.log(nama)
                    }
                });
            })



            //console.log(text);


            previewDefaultText.style.display = "none";
            previewKTP.style.display = "block";
            let tensor = await tf.browser.fromPixels(image);
            const height = tensor.shape[0];
            const width = tensor.shape[1];
            previewKTP.setAttribute("src", this.result);
            const predictions = await model.estimateFaces(tensor, false);
            if (predictions.length > 0) {
                const pred = predictions[0];
                console.log(pred);
                const w = pred.bottomRight[0] - pred.topLeft[0];
                const h = pred.bottomRight[1] - pred.topLeft[1];
                const x = pred.topLeft[0];
                const y = pred.topLeft[1];

                console.log(x)
                console.log(y)
                const canvas = document.createElement("canvas");
                canvas.style.display = "none";
                canvas.width = width;
                canvas.height = height;
                const ctx = canvas.getContext("2d");
                ctx.drawImage(image, x, y, w, h, 0, 0, width, height);

                const jpegUrl = canvas.toDataURL("image/jpeg");
                //previewKTP.setAttribute("src", jpegUrl);
                previewFaceDefaultText.style.display = "none";
                previewFace.style.display = "block";
                //previewKTP.style.width = w;
                //previewFace.style.height = h;
                previewFace.setAttribute("src", jpegUrl);
            }
            //console.log(height)
            //console.log(width);
            /*
            const predictions = await model.estimateFaces(tensor, true);  
            if (predictions.length > 0) {
                const prediction = predictions[0];
                const topLeftTensor = prediction.topLeft;
                const bottomRightTensor = prediction.bottomRight;
                const dataTopLeft = await topLeftTensor.data()
                const databottomRight = await bottomRightTensor.data()
                //console.log(dataTopLeft[0])
                const w = databottomRight[0] - dataTopLeft[0];
                const hh = databottomRight[1] - dataTopLeft[1];
                console.log(hh)
                const boxes = tf.concat([topLeftTensor, bottomRightTensor]).reshape([-1, 4])
                const data = await tensor.data()
                //console.log(data);
                const tensor4d = tf.tensor4d(data, [1, height, width, 3])
                //console.log(tensor4d);
                //console.log(boxes.print());
                //const tensor4d = await tensor.expandDims(0);
                //console.log(tensor4d.print())
                const crop = tf.image.cropAndResize(tensor4d, boxes, [0], [hh, w])
                //const cropTensor3d = await crop.as3D()
                //const cropReshape = tf.reshape([crop.shape[1]])
                console.log(crop);
                //const pixels = await tf.browser.toPixels(cropTensor3d);
                //console.log(pixels)
                //
            }
            */

        });

        reader.readAsDataURL(file);
    }
});

function loadImage(url) {
    return new Promise((resolve, reject) => {
        const im = new Image();
        im.crossOrigin = 'anonymous';
        im.src = url
        im.onload = () => {
            resolve(im)
        }
    });
}