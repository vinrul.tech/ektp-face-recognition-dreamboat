const fileKtp = document.getElementById("file_ktp");
const fileFoto = document.getElementById("file_foto");

const previewContainer = document.getElementById("ktpPreview");
const previewKTP = previewContainer.querySelector(".image-preview__ktp");
const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

const previewFaceContainer = document.getElementById("facePreview");
const previewFace = previewFaceContainer.querySelector(".face-preview__face");
const previewFaceDefaultText = previewFaceContainer.querySelector(".face-preview__default-text");

const previewFotoContainer = document.getElementById("fotoPreview");
const previewFoto = previewFotoContainer.querySelector(".foto-preview__foto");
const previewFotoDefaultText = previewFotoContainer.querySelector(".foto-preview__default-text");

const eNIK = document.getElementById("nik");
const eNama = document.getElementById("nama");
const eProses = document.getElementById("proses");

const face_result = document.getElementById("face_result");

const btnUpload = document.getElementById("upload");

const cvs = document.getElementById("crop_canvas");

let isReadKtp = false;

function emptyImage() {
    previewDefaultText.style.display = "block";
    previewKTP.style.display = "none";
    previewFaceDefaultText.style.display = "block";
    previewFace.style.display = "none";
    previewKTP.setAttribute("src", "");
    previewFace.setAttribute("src", "");
}

function emptyFoto() {
    previewFotoDefaultText.style.display = "block";
    previewFoto.style.display = "none";
    previewFoto.setAttribute("src", "");
}


let faceMatcher;
let model;

let fileWajahKtpUpload;
let fileKtpUpload;
let fileWajahFotoUpload;

Promise.all([
    faceapi.nets.ssdMobilenetv1.loadFromUri('/weights'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/weights'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/weights'),
    faceapi.tf.loadGraphModel("./web_model/model.json")
]).then((value) => {
    //console.log(value)
    model = value[3]
    const sig = model.executor._signature;
    console.log(sig)
    Object.keys(sig['outputs']).findIndex((a) => console.log(a))
    console.log("Loaded model");
    fileKtp.addEventListener("change", async function () {
        emptyImage();
        face_result.innerHTML = "";
        if (isReadKtp) {
            M.toast({
                html: 'Masih membaca file ktp'
            });
            return
        }

        const file = this.files[0];
        if (!file) {
            return;
        }


        const reader = new FileReader();
        reader.addEventListener('load', async function () {

            const ktpCheck = await checkKtp(this.result);

            if (ktpCheck.is_ktp) {

                //const img = await faceapi.bufferToImage(file);
                const img = await faceapi.fetchImage(ktpCheck.image.urlImage)
                previewDefaultText.style.display = "none";
                previewKTP.style.display = "block";
                previewKTP.setAttribute("src", ktpCheck.image.urlImage);

                readKtp(ktpCheck.image.urlImage)

                const prediction = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
                if (prediction) {

                    const pred = prediction.detection;

                    faceMatcher = new faceapi.FaceMatcher(prediction)

                    //const jpegUrl = await cropImage(pred, ktpCheck.image);
                    const boxes = {
                        x1: pred.box.topLeft.x,
                        y1: pred.box.topLeft.y,
                        x2: pred.box.bottomRight.x,
                        y2: pred.box.bottomRight.y,
                        width: pred.box.width,
                        height: pred.box.height
                    }
                    const image = await loadImage(ktpCheck.image.urlImage)
                    const wajahKtp = await cropImage(boxes, image, "wajah_ktp.jpeg")
                    previewFaceDefaultText.style.display = "none";
                    previewFace.style.display = "block";
                    previewFace.setAttribute("src", wajahKtp.urlImage);
                    fileWajahKtpUpload = wajahKtp.fileImage;
                    fileKtpUpload = ktpCheck.image.fileImage;
                } else {
                    M.toast({
                        html: 'Foto wajah KTP tidak terdeteksi'
                    });
                }
            } else {
                M.toast({
                    html: 'KTP tidak terdeteksi'
                });
            }


        });


        reader.readAsDataURL(file);


    });

    fileFoto.addEventListener("change", async function () {
        emptyFoto();
        face_result.innerHTML = "";
        if (isReadKtp) {
            M.toast({
                html: 'Masih membaca file ktp'
            });
            return
        }
        const file = this.files[0];
        if (!file) {
            return;
        }



        const img = await faceapi.bufferToImage(file);

        const prediction = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();

        if (prediction) {
            const pred = prediction.detection;

            if (!faceMatcher) {
                M.toast({
                    html: 'Foto ektp belum di upload'
                });
            } else {
                const bestMatch = faceMatcher.findBestMatch(prediction.descriptor)
                //console.log(' distance : ', bestMatch.distance)
                if (bestMatch.distance <= 0.55) {
                    face_result.innerHTML = "Foto dengan wajah foto ktp MIRIP , distance : " + bestMatch.distance;
                } else if (bestMatch.distance <= 0.65) {
                    face_result.innerHTML = "Foto dengan wajah foto ktp HAMPIR MIRIP , distance : "  + bestMatch.distance;
                } else {
                    face_result.innerHTML = "Foto dengan wajah foto ktp TIDAK MIRIP , distance : " + bestMatch.distance;
                }
            }

            //const jpegUrl = await cropImage(pred, img.src);
            const boxes = {
                x1: pred.box.topLeft.x,
                y1: pred.box.topLeft.y,
                x2: pred.box.bottomRight.x,
                y2: pred.box.bottomRight.y,
                width: pred.box.width,
                height: pred.box.height
            }
            const wajahFoto = await cropImage(boxes, img, "wajah_foto.jpeg")
            previewFotoDefaultText.style.display = "none";
            previewFoto.style.display = "block";
            previewFoto.setAttribute("src", wajahFoto.urlImage);
            fileWajahFotoUpload = wajahFoto.fileImage;
        } else {
            M.toast({
                html: 'Wajah tidak terdeteksi'
            });
        }
    });

    btnUpload.addEventListener("click", function (event) {

        if (fileWajahKtpUpload && fileKtpUpload && fileWajahFotoUpload) {

            const data = new FormData()
            data.append('wajah', fileWajahKtpUpload)
            data.append('foto', fileWajahFotoUpload)
            data.append('ktp', fileKtpUpload)

            axios.post('http://localhost:5000/upload', data)
                .then(response => {
                    console.log(response)
                    //event.preventDefault()
                })
                .catch(error => {
                    console.log(error.response.data.message)
                })

        }


        event.preventDefault()
    });


});

function loadImage(url) {
    return new Promise((resolve, reject) => {
        const im = new Image();
        im.crossOrigin = 'anonymous';
        im.src = url
        im.onload = () => {
            resolve(im)
        }
    });
}

async function cropImage(boxes, src, filename) {

    let tensor = await faceapi.tf.browser.fromPixels(src);

    const imgHeight = tensor.shape[0];
    const imgWidth = tensor.shape[1];

    let x1 = (boxes.x1 / imgWidth);
    let y1 = (boxes.y1 / imgHeight);
    let x2 = (boxes.x2 / imgWidth);
    let y2 = (boxes.y2 / imgHeight);

    const width = Math.floor(boxes.width)
    const height = Math.floor(boxes.height)

    let crop = faceapi.tf.image.cropAndResize(
            tensor.reshape([1, imgHeight, imgWidth, 3]),
            [
                [y1, x1, y2, x2]
            ],
            [0],
            [height, width])
        .div(faceapi.tf.scalar(255))
        .squeeze();
    cvs.width = width
    cvs.height = height

    await faceapi.tf.browser.toPixels(crop, cvs)

    const imgUrl = cvs.toDataURL()

    crop.dispose()
    tensor.dispose();

    const res = await fetch(imgUrl)
    const blob = await res.blob();
    const file = new File([blob], filename, {
        type: "image/jpeg"
    })

    return {
        fileImage: file,
        urlImage: imgUrl
    }

}

async function checkKtp(file) {
    const image = await loadImage(file);
    let tensor = await faceapi.tf.browser.fromPixels(image).toInt();
    const height = tensor.shape[0];
    const width = tensor.shape[1];
    tensor = tensor.transpose([0, 1, 2]).expandDims()
    faceapi.tf.engine().startScope();
    const predictions = await model.executeAsync(tensor);

    //console.log(predictions)

    const boxes = predictions[4].arraySync();
    const scores = predictions[5].arraySync();
    const classes = predictions[6].dataSync();
    //console.log(boxes)
    const detections = renderPredictions(boxes, classes, scores, width,
        height);
    if (detections.length > 0) {

        const x = detections[0].bbox[0];
        const y = detections[0].bbox[1];
        const w = detections[0].bbox[2];
        const h = detections[0].bbox[3];

        const boxes = {
            x1: x,
            y1: y,
            x2: x + w,
            y2: y + h,
            width: w,
            height: h
        }
        //const image = await loadImage(ktpCheck.image)
        const imageKTP = await cropImage(boxes, image, "ktp.jpeg")

       

        return {
            is_ktp: true,
            image: imageKTP,
        }

    }

    tensor.dispose()
    faceapi.tf.dispose(predictions)
    faceapi.tf.engine().endScope();

    return {
        is_ktp: false,
        image: null,
    }
}

function renderPredictions(boxes, classes, scores, width, height) {
    const detectionObjects = []
    scores[0].forEach((score, i) => {
        //console.log(score, i)

        if (score[1] > 0.75) {
            //console.log(score[1])
            //console.log(predictionClasses[i])
            const bbox = [];
            const minY = boxes[0][i][0] * height;
            const minX = boxes[0][i][1] * width;
            const maxY = boxes[0][i][2] * height;
            const maxX = boxes[0][i][3] * width;
            bbox[0] = minX;
            bbox[1] = minY;
            bbox[2] = maxX - minX;
            bbox[3] = maxY - minY;
            //console.log(bbox)
            //console.log(width, height)
            console.log(classes[i])
            detectionObjects.push({
                score: score[1].toFixed(4),
                bbox: bbox
            })
        }
    });
    return detectionObjects;
}

async function readKtp(imgSrc) {
    isReadKtp = true;
    eNIK.setAttribute("value", "")
    eNama.setAttribute("value", "")
    const image = await loadImage(imgSrc);
    eProses.style.display = "block";
    Tesseract.recognize(
        image,
        'eng', {
            logger: m => console.log(m)
        }
    ).then(({
        data: {
            text
        }
    }) => {
        isReadKtp = false;
        console.log(text);
        eProses.style.display = "none";
        const arrayText = text.split("\n");
        //console.log(arrayText);
        const NUMERIC_REGEXP = /[-]{0,1}[\d]*[.]{0,1}[\d]+/g;
        arrayText.forEach((txt) => {
            const pattNIK = new RegExp("NIK");
            const tNIK = pattNIK.test(txt)
            if (tNIK) {
                //console.log(txt)
                const nik = txt.match(NUMERIC_REGEXP);
                //console.log(nik)
                if (nik.length > 0) {


                    eNIK.setAttribute("value", nik.join(''))
                    //console.log(nik[0])
                }
            }

            const pattNama = new RegExp("Nama");
            const tNama = pattNama.test(txt)
            if (tNama) {
                let nama = txt.replace('Nama', '');
                nama = nama.replace(":", '')
                nama = nama.replace("-", '')
                eNama.setAttribute("value", nama)
                //console.log(nama)
            }
        });
    })
}